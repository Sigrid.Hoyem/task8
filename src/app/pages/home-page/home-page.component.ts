import { Component, OnInit } from '@angular/core';
import { Planet } from '../../planet';
import { PlanetService } from '../../services/planet.service'

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  planets: Planet[] = [];
  
  selectedPlanet: Planet;

  constructor(private planetService: PlanetService) { }

  ngOnInit() {
    this.getPlanets();
  }

  onSelect(planet: Planet): void{
    this.selectedPlanet = planet;
  }

  getPlanets():void{
    this.planetService.getPlanets().subscribe(planets => 
      //this.planets = planets.slice(1,5));
      this.planets.push(planets[this.getRandomPlanet(planets.length)], planets[this.getRandomPlanet(planets.length)], planets[this.getRandomPlanet(planets.length)], planets[this.getRandomPlanet(planets.length)])
      );
  }

  getRandomPlanet(length){
    return Math.floor(Math.random() * length);    
  }

}
