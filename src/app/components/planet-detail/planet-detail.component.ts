import { Component, OnInit, Input } from '@angular/core';
import { Planet } from '../../planet';
import { PlanetService } from '../../services/planet.service';

@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.scss']
})
export class PlanetDetailComponent implements OnInit {

  updatePlanetToggle = false;
  
  @Input() planet: Planet;

  constructor( private planetService: PlanetService ) { }

  ngOnInit() {
  }

  toggleUpdateOn(){
    this.updatePlanetToggle = true;
  }

  cancelUpdate(){
    this.updatePlanetToggle = false;
  }

  updatePlanetInfo(): void {
    this.planetService.updatePlanet(this.planet).subscribe(() => this.cancelUpdate());
  }
  
  deletePlanetPrompt(){
    if(confirm('Are you sure you want to delete this planet?')){
      this.deletePlanet();
    }
  }
  
  deletePlanet(): void {
    this.planetService.deletePlanet(this.planet).subscribe(() => window.location.reload());
  }
}
