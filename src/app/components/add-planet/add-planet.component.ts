import { Component, OnInit } from '@angular/core';
import { Planet } from '../../planet'
import { PlanetService } from '../../services/planet.service';

@Component({
  selector: 'app-add-planet',
  templateUrl: './add-planet.component.html',
  styleUrls: ['./add-planet.component.scss']
})

export class AddPlanetComponent implements OnInit {

  planet: Planet = {
    id: 0,
    name: '',
    size: '',
    date_discovered: ''
  }

  message = '';
  planetAddedSuccess = false;

  constructor(private planetService: PlanetService) { }

  ngOnInit() {
  }

  addPlanet(){
    this.planetService.addPlanet(this.planet).subscribe(() => this.planetAdded(this.planet));
  }

  planetAdded(planet){
    this.message = 'Planet with name ' + planet.name + ' added.'
    this.planetAddedSuccess = true;
  }

}

