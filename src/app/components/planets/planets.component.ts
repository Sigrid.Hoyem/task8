import { Component, OnInit } from '@angular/core';
import { Planet } from '../../planet';
import { PlanetService } from '../../services/planet.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent implements OnInit {

  planet: Planet = {
    id: 1,
    name: 'Earth',
    size: '1Earth',
    date_discovered: 'today'
  }

  planets: Planet[];

  selectedPlanet: Planet;

  constructor(private planetService: PlanetService) { }

  ngOnInit() {
    this.getPlanets();
  }

  onSelect(planet: Planet): void{
    this.selectedPlanet = planet;
  }

  getPlanets(): void{
    this.planetService.getPlanets().subscribe(planets => this.planets = planets);
  }

}
