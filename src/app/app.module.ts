import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { AddPlanetComponent } from './components/add-planet/add-planet.component';
import { PlanetDetailComponent } from './components/planet-detail/planet-detail.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PlanetPageComponent } from './pages/planet-page/planet-page.component';
import { AddPlanetPageComponent } from './pages/add-planet-page/add-planet-page.component';

@NgModule({
  declarations: [
    AppComponent,
    PlanetsComponent,
    AddPlanetComponent,
    PlanetDetailComponent,
    HomePageComponent,
    PlanetPageComponent,
    AddPlanetPageComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
