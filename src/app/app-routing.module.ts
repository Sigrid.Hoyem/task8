import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AddPlanetPageComponent } from './pages/add-planet-page/add-planet-page.component';
import { PlanetPageComponent } from './pages/planet-page/planet-page.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch:'full'},
  {path: 'planets', component: PlanetPageComponent},
  {path: 'home', component: HomePageComponent},
  {path: 'add', component: AddPlanetPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
