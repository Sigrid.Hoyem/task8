import { Injectable } from '@angular/core';
import {Planet} from '../planet';
import {Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer ' + 'b417a1199d4b8650c44afea807877fb59e894fd651e2332617ab3f53539abf53'
  })
};


@Injectable({
  providedIn: 'root'
})
export class PlanetService {

  planetsUrl = 'https://dry-eyrie-14575.herokuapp.com/api/v1/planets';
  

  constructor(
    private http: HttpClient
  ) { }

  getPlanets(): Observable<Planet[]>{
    return this.http.get<Planet[]>(this.planetsUrl, httpOptions)
  }
  
  updatePlanet(planet: Planet): Observable<any>{
    return this.http.patch(this.planetsUrl+'/update', planet, httpOptions);
  }
  
  addPlanet(planet: Planet): Observable<Planet> {
    return this.http.post<Planet>(this.planetsUrl+'/add', planet, httpOptions);
  }

  deletePlanet(planet: Planet): Observable<Planet> {
    return this.http.delete<Planet>(this.planetsUrl+'/delete/'+planet.id, httpOptions);
  }
}
