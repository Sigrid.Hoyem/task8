export class Planet{
    id: number;
    name: string;
    size: string;
    date_discovered: string;
}